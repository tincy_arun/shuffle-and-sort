let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

function displayValueOnload() {
 let div = document.getElementById('sampleGrid');
 for(let i=0; i < arr.length; i++) {
    let shuffleTable = document.createElement('div');
    shuffleTable.innerHTML= arr[i];
    shuffleTable.classList.add('cellCol','cellCol'+ arr[i]);
    div.appendChild(shuffleTable);
 }
};
window.onload = displayValueOnload;

function shuffleCol() {
    for (let i = arr.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    resetGrid();
    displayValueOnload();
};

function resetGrid(){
    let div = document.getElementById('sampleGrid');
    div.innerHTML= "";
}

function sortTable(){
    arr.sort();
    resetGrid();
    displayValueOnload();
}